﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace PingPong_v2
{
    public partial class Form1 : Form
    {
        private int _directionX = 8;
        private int _directionY = 4;
        private int _points = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSpiel_Click(object sender, EventArgs e)
        {
            tmrSpiel.Start();
        }

        private void tmrSpiel_Tick(object sender, EventArgs e)
        {
            //Ball bewegung
            picBall.Location = new Point(picBall.Location.X + _directionX, picBall.Location.Y + _directionY);

            //Ball springt an die rechte Wand und prallt ab 
            if (picBall.Location.X >= pnlSpiel.Width - picBall.Width 
                && picBall.Location.Y + picBall.Height >= picRacketRight.Location.Y 
                && picBall.Location.Y <= picRacketRight.Location.Y + picRacketRight.Height)
            {
                _directionX = -_directionX;
                _points += 10; //es werden 10 punkte hinzugefügt
            }
            
            //game over
            if (picBall.Location.X >= pnlSpiel.Width - picBall.Width
                && (picBall.Location.Y + picBall.Height <= picRacketRight.Location.Y 
                || picBall.Location.Y >= picRacketRight.Location.Y + picRacketRight.Height))
            {
                tmrSpiel.Stop();
                string looser = "GAME OVER! Try again?";
                string caption = "GAME OVER";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result = MessageBox.Show(looser, caption, buttons);
                
                if (result == DialogResult.Yes)
                {
                    picBall.Location = new Point(379, 69);
                    _points = 0;
                }

                if (result == DialogResult.No)
                {
                    Close();
                }
            }
            
            //Ball trifft linke Wand
            if (picBall.Location.X <= 0)
            {
                _directionX = -_directionX;
            }
                        
            //Ball trifft obere Wand
            if (picBall.Location.Y >= pnlSpiel.Height - picBall.Height)
            {
                _directionY = -_directionY;
            }
            
            //Ball trifft untere Wand
            if (picBall.Location.Y < 0)
            {
                _directionY = -_directionY;
            }
            
            //Zeigt Punkestand
            txtPoints.Text = Convert.ToString(_points);
        }

        private void frmPingPong_Load(object sender, EventArgs e)
        {
            //Schläger wird an der rechten Wand plaziert
            picRacketRight.Location = new Point(pnlSpiel.Width - picRacketRight.Width - picRacketRight.Width, pnlSpiel.Height / 2);
            
            //set scrollbar next to panel, config max & min & current value
            vsbRacketRight.Height = pnlSpiel.Height;
            vsbRacketRight.Location = new Point(pnlSpiel.Location.X + pnlSpiel.Width, pnlSpiel.Location.Y);
            vsbRacketRight.Minimum = 0;
            vsbRacketRight.Maximum = pnlSpiel.Height - picRacketRight.Height + vsbRacketRight.LargeChange;
            vsbRacketRight.Value = picRacketRight.Location.Y;
        }
        
        private void vsbRacketRight_Scroll(object sender, ScrollEventArgs e)
        {
            picRacketRight.Location = new Point(picRacketRight.Location.X, vsbRacketRight.Value);
            vsbRacketRight.Value = picRacketRight.Location.Y;
        }
    }
}